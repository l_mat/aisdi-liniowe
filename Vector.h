#ifndef AISDI_LINEAR_VECTOR_H
#define AISDI_LINEAR_VECTOR_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <iostream> // testy

namespace aisdi
{

template <typename Type>
class Vector
{
public:
  using difference_type = std::ptrdiff_t;
  using size_type = std::size_t;
  using value_type = Type;
  using pointer = Type*;
  using reference = Type&;
  using const_pointer = const Type*;
  using const_reference = const Type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;
private:
  static const size_type VECTOR_ALLOC_SIZE = 5; // count of items allocated when Vector's capacity is reached
  pointer __storage; // dynamically allocated array
  size_type __size; // count of allocated cells
  size_type __length; // count of actually used storage cells
  size_type __offs; // physical index of "element 0" in the storage array
  //void resize(size_type new_size);
public:
  Vector() : __size(0)
  {
    resize(VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
  }

  Vector(std::initializer_list<Type> l) : __size(0)
  {
    resize(l.size(),VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
    for(auto it = l.begin(); it != l.end(); ++it)
      append(*it);
  }

  // l-value copy constructor
  // - copy values but not the structure of the buffer
  Vector(const Vector& other)
  {
    __size = 0; // __storage not yet initialized
    resize(other.__length,VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
    __length = other.__length;
    __offs = 0; // safe value but no spare cells before the content
    // copy the content of the other vector to this
    for(size_type i=0; i<__length; i++)
      __storage[i] = other.__storage[i + other.__offs];
  }

  // destructive r-value constructor
  // - steal the buffer from the other object
  Vector(Vector&& other)
  {
    __length = other.__length;
    __size = other.__size;
    __offs = other.__offs;
    __storage = other.__storage;
    other.__storage = nullptr;
    other.__length = 0;
  }

  ~Vector()
  {
      delete __storage;
  }

  Vector& operator=(const Vector& other)
  {
    if(other.__length > __size)
      resize(other.__length,VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
    __offs = 0; // safe value but no spare cells before the content
    __length = other.__length;
    for(size_type i=0; i<__length; i++)
      __storage[i] = other.__storage[i + other.__offs];
    return *this;
  }

  // destructive r-value assignment operator
  Vector& operator=(Vector&& other)
  {
    __length = other.__length;
    __size = other.__size;
    __offs = other.__offs;
    __storage = other.__storage;
    other.__storage = nullptr;
    other.__length = 0;
    return *this;
  }

  bool isEmpty() const
  {
    return (__length == 0);
  }

  size_type getSize() const
  {
    return __length;
  }

  // insert an item at the end of the Vector
  void append(const Type& item)
  {
    // vector is full
    if(__length + __offs == __size)
      resize(__length,VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
    // append the item to the list
    __storage[__offs + __length] = item;
    ++__length;
  }

  void prepend(const Type& item)
  {
    if(__offs == 0) {
      // no free cells in the beginning of the __storage array
      resize(__length,VECTOR_ALLOC_SIZE, VECTOR_ALLOC_SIZE);
    }
    // put item to the cell before the first used
    __storage[--__offs] = item;
    ++__length;
  }

  void dump() {
    std::cout << std::endl << "DUMP length/size/offs " << __length << "/" << __size<<"/" << __offs;
    for(size_type i = 0; i<__length;i++)
      std::cout << std::endl << i << "\t" << __storage[i + __offs] << " ";
  }

  // when requested, allocate a new bigger array and copy existing content
  // resize a list so that it has length elements at position __offs
  // and before/after cells free before/after the content
  void resize(size_type n_elem, size_type before, size_type after) {
    if(__size == 0) { // initialize the vector
      __length = 0;
      __offs = 0;
    }
    size_type free_after = __size - __offs - __length;
    size_type total = before + n_elem + after;

    // ensure enough space before/after
    // you may shift the content, but never shrink the storage
    if(__offs < before || free_after < after || __size < total) {
      pointer new_array = new Type[total]; // new storage array
      if(__size > 0) {// size 0 means that __storage was not yet initialized
        for(size_type i=0; i<__length; i++) // copy only used cells
          new_array[before + i] = __storage[__offs + i];
        delete[] __storage;
      }
      __offs = before;
      __storage = new_array;
      __size = total;
    }
  }

  // shifts all elements after position by offset
  // doesn't affect elements before position
  void shift(const const_iterator& position, difference_type offset) {

    size_type index = position.ptr - __storage;

    if(__length < 1) throw std::out_of_range("Trying to move elements of an empty list.");

    if(offset < 0) { // just erase some items - no reallocation needed
      for(; index < __length + __offs; index++) {
        std::cout << "["<<index<<"]";
        __storage[index] = __storage[index - offset]; // offset is negative
      }
    } else {
      // shift some items right
      // WARNING - doesn't handle the storage cells between position and +offset
      // allocate memory for new items if needed
      if(__offs + __length + offset < __size) {
        resize(__length + offset,VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
      } // else there is enough space
      for(size_type i = index + (__length + __offs - index); i >= index; i--) {
        __storage[i + offset] = __storage[i];
      }
    }
    __length += offset;
  }

  void insert(const const_iterator& insertPosition, const Type& item)
  {
    difference_type index = insertPosition.ptr - __storage;
    if(index < 0 || index >= (difference_type)__size)
      throw std::out_of_range("Vector index out of storage boundaries.");
    if((size_type)index < __offs || (size_type)index > __offs+__length)
      throw std::out_of_range("Vector index out of content boundaries.");

    // neither list is empty nor insertion after the last
    if(__length != 0 && insertPosition != end()-1) {
      shift(insertPosition,1);
      __storage[index] = item;
    } else {
      if(__length + __offs == __size) // vector full to the right
        resize(__length,VECTOR_ALLOC_SIZE,VECTOR_ALLOC_SIZE);
      __length++;
      __storage[index] = item;
    }
  }

  Type popFirst()
  {
    if(__length > 0) {
      __length--;
      return __storage[__offs++];
    } else throw std::logic_error("Trying to pop an item from an empty collection.");
  }

  Type popLast()
  {
    if(__length > 0) {
      __length--;
      return __storage[__offs + __length];
    }
    else throw std::logic_error("Trying to pop an item from an empty collection.");
  }

  // remove a single element at position
  void erase(const const_iterator& position)
  {
    if(position == end())
      throw std::out_of_range("Trying to erase after the end of the Vector.");
    shift(position,-1);
  }

  // remove adjacent elements between iterators
  void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
  {
    // find out how many elements to erase and do it starting from firstIncluded
    // using function shift
    difference_type n_erase = lastExcluded.ptr - firstIncluded.ptr;
    std::cout <<" n_erase="<< n_erase << " ";
    shift(firstIncluded,-n_erase);
  }

  // return an iterator to beginning of the Vector with the range set
  iterator makeIterator() {
    iterator it;
    it.p_begin = &__storage[__offs];
    it.p_end = &__storage[__offs+__length];
    it.ptr = it.p_begin;
    return it;
  }

  iterator begin()
  {
    return makeIterator();
  }

  iterator end()
  {
    iterator result = makeIterator();
    result.ptr = result.p_end;
    return result;
  }

  const_iterator cbegin() const
  {
    const_iterator it;
    it.p_begin = &__storage[__offs];
    it.p_end = &__storage[__offs+__length];
    it.ptr = it.p_begin;
    return it;
  }

  const_iterator cend() const
  {
    const_iterator result = cbegin();
    result.ptr = result.p_end;
    return result;
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }
};

template <typename Type>
class Vector<Type>::ConstIterator
{
public:
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename Vector::value_type;
  using difference_type = typename Vector::difference_type;
  using pointer = typename Vector::const_pointer;
  using reference = typename Vector::const_reference;

  friend class Vector<Type>;
private:
  pointer ptr; // pointer to the current position in the table
  // defining the range of ptr
  pointer p_begin, p_end;
public:
  explicit ConstIterator() : ptr(nullptr), p_begin(nullptr), p_end(nullptr)
  {}

  reference operator*() const
  {
    if(ptr < p_begin || ptr >= p_end)
      throw std::out_of_range("Dereferencing Vector iterator: out of range.");
    return *ptr;
  }

  // pre-increment operator
  ConstIterator& operator++()
  {
    if(ptr == p_end)
      throw std::out_of_range("Vector iterator++: out of range.");
    ptr++;
    return *this;
  }

  // post-increment operator
  // do pre-increment operation on a copy of *this
  ConstIterator operator++(int)
  {
    ConstIterator tmp(*this);
    operator++();
    return tmp;
  }

  ConstIterator& operator--()
  {
    if(ptr == p_begin)
      throw std::out_of_range("Vector iterator--: out of range.");
    ptr--;
    return *this;
  }

  ConstIterator operator--(int)
  {
    ConstIterator tmp(*this);
    operator--();
    return tmp;
  }

  ConstIterator operator+(difference_type d) const
  {
    ConstIterator result(*this);
    result.ptr += d;
    return result;
  }

  ConstIterator operator-(difference_type d) const
  {
    ConstIterator result(*this);
    result.ptr -= d;
    return result;
  }

  bool operator==(const ConstIterator& other) const
  {
    return ptr == other.ptr;
  }

  bool operator!=(const ConstIterator& other) const
  {
    return ptr != other.ptr;
  }
};

template <typename Type>
class Vector<Type>::Iterator : public Vector<Type>::ConstIterator
{
public:
  using pointer = typename Vector::pointer;
  using reference = typename Vector::reference;

  explicit Iterator()
  {}

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  Iterator operator+(difference_type d) const
  {
    return ConstIterator::operator+(d);
  }

  Iterator operator-(difference_type d) const
  {
    return ConstIterator::operator-(d);
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

}

#endif // AISDI_LINEAR_VECTOR_H
