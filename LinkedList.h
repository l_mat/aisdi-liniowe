#ifndef AISDI_LINEAR_LINKEDLIST_H
#define AISDI_LINEAR_LINKEDLIST_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <iostream>

namespace aisdi
{

template <typename Type>
class LinkedList
{
public:
  using difference_type = std::ptrdiff_t;
  using size_type = std::size_t;
  using value_type = Type;
  using pointer = Type*;
  using reference = Type&;
  using const_pointer = const Type*;
  using const_reference = const Type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;
  struct Node; // Node structure for LinkedList
private:
   size_type __size; // current length of the list
   // sentinel node to help implementing iterator at next after the end node
   // - sentinel->next is always nullptr
   // - sentinel->prev == nullptr indicates empty list
   Node* sentinel;
   Node* head; // pointer to the first element

  // helper function used only in constructors
  // initialize the list so that it contains nothing
  // initialize sentinel which is always present
  void init() {
    sentinel = new Node;
    sentinel->next = nullptr; // always
    sentinel->prev = nullptr;
    head = sentinel;
    __size = 0;
  }
public:
  LinkedList()
  {
    init();
  }

  // initialize the LinkedList with array of elements
  LinkedList(std::initializer_list<Type> l)
  {
    init();
    for(auto it = l.begin(); it != l.end(); ++it)
      append(*it);
  }

  LinkedList(const LinkedList& other)
  {
    init();
    for(auto it = other.cbegin(); it != other.cend(); ++it)
      append(*it);
  }

  // steal content from the other LinkedList
  LinkedList(LinkedList&& other)
  {
    sentinel = other.sentinel;
    head = other.head;
    __size = other.__size;
    other.__size = 0;
    other.sentinel = nullptr;
  }

  ~LinkedList()
  { // free the allocated nodes
      erase(begin(),end());
      if(sentinel)
        delete sentinel;
  }

  // copy the content of the other list
  LinkedList& operator=(const LinkedList& other)
  {
    // clean the current items
    erase(cbegin(),cend());
    for(auto it = other.cbegin(); it != other.cend(); ++it)
      append(*it);
    return *this;
  }

  // destructive copy constructor - steal data from r-value
  LinkedList& operator=(LinkedList&& other)
  {
    sentinel = other.sentinel;
    head = other.head;
    __size = other.__size;
    other.__size = 0;
    other.sentinel = nullptr;
    return *this;
  }

  bool isEmpty() const
  {
    return __size < 1;
  }

  // get current number of items of the list
  size_type getSize() const
  {
    return __size;
  }

  // insert an item at the end of the list
  void append(const Type& item)
  {
    Node* new_node = new Node(item, sentinel->prev, sentinel);
    if(!sentinel->prev) { // list was empty
      head = new_node;
    } else {
      new_node->prev->next = new_node;
    }
    sentinel->prev = new_node;
    __size++;
  }

  // insert an item at the beginning of the list
  void prepend(const Type& item)
  {
    if(!sentinel->prev) {// list empty
      append(item);
      return;
    }
    Node *new_node = new Node(item, nullptr, head);
    head = new_node->next->prev = new_node;
    __size++;
  }

  // insert before element at the position insertPosition
  void insert(const const_iterator& insertPosition, const Type& item)
  {
    Node* p = insertPosition.ptr;
    if(p->prev) {
      Node* new_node = new Node(item, p->prev, p);
      p->prev = new_node;
      new_node->prev->next = new_node;
      __size++;
    } else { // put the element to the beginning
      prepend(item);
    }
  }

  // remove the first element and return its value
  Type popFirst()
  {
    if(!sentinel->prev) // list empty
      throw std::out_of_range("Trying to popFirst() an element from an empty list.");
    Node* tmp = head;
    Type result = head->data;
    head = head->next;
    __size--;
    delete tmp;
    return result;
  }

  // remove the last element and return its value
  Type popLast()
  {
    if(!sentinel->prev) // list empty
      throw std::out_of_range("Trying to popLast() an element from an empty list.");
    Node* tmp = sentinel->prev;
    Type result = tmp->data;
    sentinel->prev = tmp->prev;
    if(tmp->prev)
      tmp->prev->next = sentinel;
    else // the only item
      head = sentinel;
    __size--;
    delete tmp;
    return result;
  }

  void erase(const const_iterator& position)
  {
    Node* tmp = position.ptr;
    if(tmp == nullptr || tmp == sentinel)
      throw std::out_of_range("Trying to erase LinkedList out of range.");

    tmp->next->prev = tmp->prev;
    if(tmp->prev)
      tmp->prev->next = tmp->next;
    else
      head = tmp->next;

    delete tmp;
    __size--; // if successful
  }

  void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
  {
    for(const_iterator it = firstIncluded; it != lastExcluded; ++it)
      erase(it);
  }

  // return an iterator pointing to the beginning of the list
  iterator begin()
  {
    return iterator(head);
  }

  // iterator pointing after the end of the list
  iterator end()
  {
    return iterator(sentinel);
  }

  const_iterator cbegin() const
  {
    return const_iterator(head);
  }

  const_iterator cend() const
  {
    return const_iterator(sentinel);
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }
};

template <typename Type>
class LinkedList<Type>::ConstIterator
{
public:
  Node* ptr; // the current element
public:
  friend class LinkedList;
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename LinkedList::value_type;
  using difference_type = typename LinkedList::difference_type;
  using pointer = typename LinkedList::const_pointer;
  using reference = typename LinkedList::const_reference;

  explicit ConstIterator()
  {}

  explicit ConstIterator(Node *node)
  {
    ptr = node;
  }

  reference operator*() const
  {
    if(ptr->next) // not the sentinel node
      return ptr->data;
    else
      throw std::out_of_range("Trying to dereference a sentinel node (?).");
  }

  ConstIterator& operator++()
  {
    if(ptr->next == nullptr)
      throw std::out_of_range("Iterator++: end reached.");
    else
      ptr = ptr->next;
    return *this;
  }

  ConstIterator operator++(int)
  {
    ConstIterator result(ptr);
    operator++();
    return result;
  }

  ConstIterator& operator--()
  {
    if(ptr && ptr->prev)
      ptr = ptr->prev;
    else
      throw std::out_of_range("Iterator-- out of range.");
    return *this;
  }

  ConstIterator operator--(int)
  {
    ConstIterator result(ptr);
    operator--();
    return result;
  }

  ConstIterator operator+(difference_type d) const
  {
    ConstIterator result(ptr);
    while(d--)
      ++result;
    return result;
  }

  ConstIterator operator-(difference_type d) const
  {
    ConstIterator result(ptr);
    while(d--)
      --result;
    return result;
  }

  bool operator==(const ConstIterator& other) const
  {
   return ptr == other.ptr;
  }

  bool operator!=(const ConstIterator& other) const
  {
   return ptr != other.ptr;
  }
};

template <typename Type>
class LinkedList<Type>::Iterator : public LinkedList<Type>::ConstIterator
{
public:
  using pointer = typename LinkedList::pointer;
  using reference = typename LinkedList::reference;

  explicit Iterator()
  {}

  explicit Iterator(Node* node)
  {
    ConstIterator::ptr = node;
  }

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  Iterator operator+(difference_type d) const
  {
    return ConstIterator::operator+(d);
  }

  Iterator operator-(difference_type d) const
  {
    return ConstIterator::operator-(d);
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

// node structure for LinkedList
template <typename Type>
struct LinkedList<Type>::Node
{
  Node() : prev(nullptr), next(nullptr) {};
  Node( const Type data0, Node* p = nullptr, Node* n = nullptr )
    : data(data0), prev(p), next(n) { }

  Type data;
  Node* prev;
  Node* next;
};

}

#endif // AISDI_LINEAR_LINKEDLIST_H
